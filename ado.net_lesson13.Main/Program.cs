﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbUp;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;

namespace ado.net_lesson13.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionStringConfiguration = ConfigurationManager.ConnectionStrings["appConnection"];
            var connectionString = connectionStringConfiguration.ConnectionString;
            //var providerName = connectionStringConfiguration.ProviderName;

            #region migration
            EnsureDatabase.For.SqlDatabase(connectionString);

            var upgrader =
            DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful) throw new Exception("Migration not successful");
            #endregion


            //Выполняем стандартные запросы

            Console.ReadLine();
        }
    }
}
